package Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import Entity.Empleado;
import Services.EmpleadoServices;

@RestController
@RequestMapping (path = "/empleado")
public class EmpleadoController {

	@Autowired
	private EmpleadoServices empSer;
	
	@CrossOrigin
	@ResponseBody
	@PostMapping (path = "/guardar")
	public Empleado crearEmpleado (@RequestBody Empleado emp) {
		return empSer.creaEmpleado(emp);
	}
	
	@GetMapping(path = "/listar")
	public List<Empleado> listar(){
		return empSer.listar();
	}
	
	@GetMapping(path = "/listabyId")
	public Empleado listabyId(@RequestParam (name = "id") String id){
		return empSer.listabyId(id);
	}
	
	@GetMapping(path = "/listabyNombre")
	public List<Empleado> listabyNombre(@RequestParam (name = "nombre") String nombre){
		return empSer.listaporNombre(nombre);
	}
	
	@GetMapping(path = "/listabyVacuna")
	public List<Empleado> listabyVacuna(@RequestParam (name = "vacuna") String vac){
		return empSer.listaporVacuna(vac);
	}
	
	@DeleteMapping("eliminar({id}")
	public ResponseEntity<?>delete(@PathVariable String id){
		Empleado emp = empSer.listabyId(id);
		if (emp == null) {
			ResponseEntity.badRequest().build();
		}
		empSer.eliminarporId(id);
		return ResponseEntity.ok().build();
	}
	
	@PutMapping (value = "editar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus (HttpStatus.OK)
	public ResponseEntity<Empleado> updateEmpleado ( @PathVariable (value = "id") String id, @RequestBody Empleado emp) {
		Empleado empActualiza = empSer.listabyId(id);
		if (empActualiza == null) {
			ResponseEntity.badRequest().build();
		}
		empActualiza.setEmp_names(emp.getEmp_names());
		empActualiza.setEmp_apellidos(emp.getEmp_apellidos());
		empActualiza.setEmp_dir(emp.getEmp_dir());
		empActualiza.setEmp_tel(emp.getEmp_tel());
		empActualiza.setEmp_est_vac(emp.getEmp_est_vac());
		empActualiza.setEmp_fec_nac(emp.getEmp_fec_nac());
		empActualiza.setEmp_mail(emp.getEmp_mail());
		empActualiza.setEmp_rol(emp.getEmp_rol());
		return ResponseEntity.ok(empSer.creaEmpleado(empActualiza));
	}
}
