package Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import Entity.Vacuna;
import Services.VacunaService;

@RestController
@RequestMapping(path = "/vacuna")
public class VacunaController {

	@Autowired
	private VacunaService vacSer;
	
	@CrossOrigin
	@ResponseBody
	@PostMapping (path = "/guardar")
	public Vacuna crearVacuna (@RequestBody Vacuna emp) {
		return vacSer.creaVacuna(emp);
	}
	
	@GetMapping(path = "/listar")
	public List<Vacuna> listar(){
		return vacSer.listar();
	}
	
	@GetMapping(path = "/listabyId")
	public Vacuna listabyId(@RequestParam (name = "id") Integer id){
		return vacSer.listarbyId(id);
	}
	
	@GetMapping(path = "/listabyEmpleado")
	public List<Vacuna> listabyEmpleado(@RequestParam (name = "emp") String emp){
		return vacSer.listarByEmpleado(emp);
	}
	
	@GetMapping(path = "/listabyTipo")
	public List<Vacuna> listabyTipo(@RequestParam (name = "tipo") String tipo){
		return vacSer.listarByTipo(tipo);
	}
	
	@PutMapping (value = "editar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus (HttpStatus.OK)
	public ResponseEntity<Vacuna> updateVacuna ( @PathVariable (value = "id") Integer id, @RequestBody Vacuna vac) {
		Vacuna vacActualiza = vacSer.listarbyId(id);
		if (vacActualiza == null) {
			ResponseEntity.badRequest().build();
		}
		vacActualiza.setEmp_id(vac.getEmp_id());
		vacActualiza.setVac_dosis(vac.getVac_dosis());
		vacActualiza.setVac_fec(vac.getVac_fec());
		vacActualiza.setVac_tipo(vac.getVac_tipo());
		return ResponseEntity.ok(vacSer.creaVacuna(vacActualiza));
	}
}
