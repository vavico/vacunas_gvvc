package Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import Entity.Usuario;
import Services.UsuarioService;


@RestController
@RequestMapping(path = "/usuario")
public class UsuarioController {

	@Autowired
	private UsuarioService usuSer;
	
	@CrossOrigin
	@ResponseBody
	@PostMapping (path = "/guardar")
	public Usuario crearUsuario (@RequestBody Usuario emp) {
		return usuSer.creaUsuario(emp);
	}
	
	@GetMapping(path = "/listar")
	public List<Usuario> listar(){
		return usuSer.listar();
	}
	
	@GetMapping(path = "/listabyId")
	public Usuario listabyId(@RequestParam (name = "id") Integer id){
		return usuSer.listarporId(id);
	}
	
	@GetMapping(path = "/listabyEmpleado")
	public List<Usuario> listabyEmpleado(@RequestParam (name = "emp") String emp){
		return usuSer.listarporEmpleado(emp);
	}
	
	@GetMapping(path = "/listabyTipo")
	public List<Usuario> listabyRol(@RequestParam (name = "rol") String rol){
		return usuSer.listarporRol(rol);
	}
	
	@PutMapping (value = "editar/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseStatus (HttpStatus.OK)
	public ResponseEntity<Usuario> updateUsuario ( @PathVariable (value = "id") Integer id, @RequestBody Usuario vac) {
		Usuario usuActualiza = usuSer.listarporId(id);
		if (usuActualiza == null) {
			ResponseEntity.badRequest().build();
		}
		usuActualiza.setUsu_emp(vac.getUsu_emp());
		usuActualiza.setUsu_pw(vac.getUsu_pw());
		usuActualiza.setUsu_rol(vac.getUsu_rol());
		usuActualiza.setUsu_user(vac.getUsu_user());
		return ResponseEntity.ok(usuSer.creaUsuario(usuActualiza));
	}
}
