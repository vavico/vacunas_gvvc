package Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Entity.Usuario;
import Repository.UsuarioRepository;

@Service
public class UsuarioService {
	
	@Autowired 
	private UsuarioRepository usuRepo;
	
	//guardar
	public Usuario creaUsuario (Usuario usu) {
		return usuRepo.save(usu);
	}
	
	//listar
	public List<Usuario> listar() {
		return usuRepo.findAll();
	}
	
	//listar por Id
	public Usuario listarporId(Integer id) {
		return usuRepo.FiltrarbyId(id);
	}
	
	//Listar por Empleado
	public List<Usuario> listarporEmpleado(String emp) {
		return usuRepo.findbyEmpleado(emp);
	}
	
	//Listar por Rol
	public List<Usuario> listarporRol (String rol) {
		return usuRepo.findbyRol(rol);
	}
}
