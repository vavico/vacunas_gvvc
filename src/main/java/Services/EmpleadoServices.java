package Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Entity.Empleado;
import Repository.EmpleadoRepository;

@Service
public class EmpleadoServices {

	@Autowired
	private EmpleadoRepository empRepo;
	
	//guardar
	public Empleado creaEmpleado (Empleado emp) {
		return empRepo.save(emp);
	}
	
	//listar
	public List<Empleado> listar(){
		return empRepo.findAll();
	}
	
	//Lista por Id
	public Empleado listabyId(String emp_id) {
		return empRepo.FiltrarPorId(emp_id);
	}
	
	//Lista por nombres o apellidos
	public List<Empleado> listaporNombre(String text){
		return empRepo.findbyNombre(text);
	}
	
	//Lista por Estado de Vacuna
	public List<Empleado> listaporVacuna(String text) {
		return empRepo.findbyEstadoVacuna(text);
	}
	
	//Eliminar por ID
	public void eliminarporId (String id) {
		empRepo.deleteById(id);
	}
}
