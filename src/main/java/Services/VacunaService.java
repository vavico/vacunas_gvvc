package Services;

import java.sql.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import Entity.Vacuna;
import Repository.VacunaRepository;

@Service
public class VacunaService {

	@Autowired
	private VacunaRepository vacRepo;
	
	//guardar
	public Vacuna creaVacuna(Vacuna vac) {
		return vacRepo.save(vac);
	}
	
	//listar
	public List<Vacuna> listar(){
		return vacRepo.findAll();
	}
	
	//listar por ID
	public Vacuna listarbyId(Integer id){
		return vacRepo.FiltrarbyId(id);
	}
	
	//listar por Empleado
	public List<Vacuna> listarByEmpleado (String emp){
		return vacRepo.findbyEmpleado(emp);
	}
	
	//listar por Tipo
	public List<Vacuna> listarByTipo(String tipo) {
		return vacRepo.findbyTipo(tipo);
	}
	
	//listar por Fecha Vacuna
	public List<Vacuna> listaByFecVacuna (Date fVac) {
		return vacRepo.findbyFechaVac(fVac);
	}
}
