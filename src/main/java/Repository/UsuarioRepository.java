package Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import Entity.Usuario;

@Repository
public interface UsuarioRepository extends JpaRepository<Usuario, Integer> {
	
	//Buscar por Id
	@Query (value = "select u from usuario u where u.usu_id = ?", nativeQuery = true)
	Usuario FiltrarbyId(Integer usu_id);
	
	//Buscar por usuario
	@Query(value = "select u from usuario u where u.usu_user like %:text% or e.emp_apellidos like %:text%")
    List<Usuario> findbyUsuario(@Param("text") String text);
	
	//Buscar por empleado
	@Query(value = "select e from empleado e where e.usu_emp like %:text% or e.emp_apellidos like %:text%")
    List<Usuario> findbyEmpleado(@Param("text") String text);
	
	//Buscar por Rol
	@Query(value = "select e from empleado e where e.usu_rol like %:text% or e.emp_apellidos like %:text%")
    List<Usuario> findbyRol(@Param("text") String text);
}
