package Repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import Entity.Vacuna;

@Repository
public interface VacunaRepository extends JpaRepository<Vacuna, Integer> {

	//Buscar por Id
	@Query (value = "select v from vacuna v where v.vac_id = ?", nativeQuery = true)
	Vacuna FiltrarbyId(Integer vac_id);
	
	//Buscar por Empleado
	@Query(value = "select v from vacuna v where v.emp_id like %:text%")
    List<Vacuna> findbyEmpleado(@Param("text") String text);
	
	//Buscar por Tipo
	@Query(value = "select v from vacuna v where v.vac_tipo = :text")
    List<Vacuna> findbyTipo(@Param("text") String text);
	
	//Buscar por Fecha Vacuna
	@Query(value = "select v from vacuna v where v.vac_fec = :text")
    List<Vacuna> findbyFechaVac(@Param("text") Date text);
}
