package Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import Entity.Empleado;

@Repository
public interface EmpleadoRepository extends JpaRepository<Empleado, String>{
	//Buscar por Id
	@Query(value = "select e from empleado e where e.emp_id = ?", nativeQuery = true)
	Empleado FiltrarPorId(String emp_id);
	
	
	//Eliminar por Id
	@Transactional
    @Modifying
    @Query(value = "delete from empleado where emp_id=?", nativeQuery =true)
    void deleteById(String emp_id);
	
	//Buscar por nombres o apellidos
	@Query(value = "select e from empleado e where e.emp_nombres like %:text% or e.emp_apellidos like %:text%")
    List<Empleado> findbyNombre(@Param("text") String text);
	
	//Buscar por Estado de Vacuna
	@Query(value = "select e from empleado e where e.emp_est_vac like %:text% or e.emp_apellidos like %:text%")
    List<Empleado> findbyEstadoVacuna(@Param("text") String text);
	
}
