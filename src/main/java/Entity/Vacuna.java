package Entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table (name = "VACUNA")
public class Vacuna implements Serializable {
	@Id
    @Column(name="vac_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY, generator = "VacunaTable")
    private int vac_id;
    @Column(name = "emp_id")
    @JoinColumn(name = "emp_id", referencedColumnName = "emp_id")
    private String emp_id;
    @Column(name = "vac_dosis")
    private int vac_dosis;
    @Column (name = "vac_tipo")
    private Empleado vac_tipo;
    @Column (name = "vac_fec")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vac_fec;
	public int getVac_id() {
		return vac_id;
	}
	public void setVac_id(int vac_id) {
		this.vac_id = vac_id;
	}
	public String getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(String emp_id) {
		this.emp_id = emp_id;
	}
	public int getVac_dosis() {
		return vac_dosis;
	}
	public void setVac_dosis(int vac_dosis) {
		this.vac_dosis = vac_dosis;
	}
	public Empleado getVac_tipo() {
		return vac_tipo;
	}
	public void setVac_tipo(Empleado vac_tipo) {
		this.vac_tipo = vac_tipo;
	}
	public Date getVac_fec() {
		return vac_fec;
	}
	public void setVac_fec(Date vac_fec) {
		this.vac_fec = vac_fec;
	}
    
}
