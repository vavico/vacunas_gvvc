package Entity;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table (name = "EMPLEADO")
public class Empleado implements Serializable {
	@Id
    @Column(name="emp_id")
    private int emp_id;
    @Column(name = "emp_names", unique=true)
    private String emp_names;
    @Column(name = "emp_apellidos")
    private String emp_apellidos;
    @Column (name = "emp_mail")
    private Empleado emp_mail;
    @Column(name = "emp_fec_nac")
    private Date emp_fec_nac;
    @Column(name = "emp_dir")
    private String emp_dir;
    @Column(name = "emp_tel")
    private String emp_tel;
    @Column(name = "emp_est_vac")
    private char emp_est_vac;
    @Column (name = "emp_rol")
    private String emp_rol;
	public int getEmp_id() {
		return emp_id;
	}
	public void setEmp_id(int emp_id) {
		this.emp_id = emp_id;
	}
	public String getEmp_names() {
		return emp_names;
	}
	public void setEmp_names(String emp_names) {
		this.emp_names = emp_names;
	}
	public String getEmp_apellidos() {
		return emp_apellidos;
	}
	public void setEmp_apellidos(String emp_apellidos) {
		this.emp_apellidos = emp_apellidos;
	}
	public Empleado getEmp_mail() {
		return emp_mail;
	}
	public void setEmp_mail(Empleado emp_mail) {
		this.emp_mail = emp_mail;
	}
	public Date getEmp_fec_nac() {
		return emp_fec_nac;
	}
	public void setEmp_fec_nac(Date emp_fec_nac) {
		this.emp_fec_nac = emp_fec_nac;
	}
	public String getEmp_dir() {
		return emp_dir;
	}
	public void setEmp_dir(String emp_dir) {
		this.emp_dir = emp_dir;
	}
	public String getEmp_tel() {
		return emp_tel;
	}
	public void setEmp_tel(String emp_tel) {
		this.emp_tel = emp_tel;
	}
	public char getEmp_est_vac() {
		return emp_est_vac;
	}
	public void setEmp_est_vac(char emp_est_vac) {
		this.emp_est_vac = emp_est_vac;
	}
	public String getEmp_rol() {
		return emp_rol;
	}
	public void setEmp_rol(String emp_rol) {
		this.emp_rol = emp_rol;
	}
}
