package Entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "USUARIO")
public class Usuario implements Serializable{
	@Id
    @Column(name="usu_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "UsuarioTable")
    private int usu_id;
    @Column(name = "usu_user", unique=true)
    private String usu_user;
    @Column(name = "usu_pw")
    private String usu_pw;
    @Column (name = "usu_emp")
    @OneToMany(mappedBy="empleado")
    @JoinColumn(name = "usu_emp", referencedColumnName = "emp_id")
    private Empleado usu_emp;
    @Column(name = "usu_rol")
    private String usu_rol;
	public int getUsu_id() {
		return usu_id;
	}
	public void setUsu_id(int usu_id) {
		this.usu_id = usu_id;
	}
	public String getUsu_user() {
		return usu_user;
	}
	public void setUsu_user(String usu_user) {
		this.usu_user = usu_user;
	}
	public String getUsu_pw() {
		return usu_pw;
	}
	public void setUsu_pw(String usu_pw) {
		this.usu_pw = usu_pw;
	}
	public Empleado getUsu_emp() {
		return usu_emp;
	}
	public void setUsu_emp(Empleado usu_emp) {
		this.usu_emp = usu_emp;
	}
	public String getUsu_rol() {
		return usu_rol;
	}
	public void setUsu_rol(String usu_rol) {
		this.usu_rol = usu_rol;
	}
}
