# Test_GV_BackEnd

***

## General Info

ApiRest para un sistema para un control de vacunas a los empleados de la empresa

## Clases

1. Empleado: Se crea un CRUD para manejo de la información de los empleados de la empresa
2. Usuario: Se crea un CRUD para el manejo de los usuarios, enlazado al usuario
3. Vacuna: Se crea un CRUD para el manejo de la información de las vacunas que han recibido los empleados
Se utiliza arquitectura MVC para poder tener un mejor orden de la ApiRest para la empresa

## Modelo de Datos

![Estructura del Modelo de Datos](/src/main/resources/modelo_datos.png)
***

## Proceso de Construcción

1. Creación de la Base de Datos
2. Creación de una aplicación con propiedades, Maven, JPA, Rest que se conecte con base de Datos PostgreSQL.
3. Modificación del Archivo Pom para que se pueda ejecutar con Swagger-OpenApi.
4. Configuración del archivo application.properties, para conectarlo con la Base de Datos creada en PostgreSql.
5. Creación de las entidades requeridas para el sistema ApiRest de vacunación.
6. Creación de las interfaces de Repositorio de cada una de las entidades.
7. Creación de los servicios de cada una de las entidades.
8. Creación de los controladores de cada una de las entidades.

***
